var user = process.env.users;

var signUp = async (data) => {
  try {
    await user.insertOne(data)
  } catch (e) {
    console.log(e);
  }
}

var login = async (data) => {
  try {
    await user.update();
  } catch (e) {
    console.log(e);
  }
}

var logout = async () => {
  try {
    await user.update();
  } catch (e) {
    console.log(e);
  }
}

module.exports = {
  signUp,
  login,
  logout
};
