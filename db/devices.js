var getAllDevices = async () => {
  try {
    await devices.find();
  } catch (e) {
    console.log(e);
  }
}

var addDevices = async (req) => {
  let {
    app,
    body
  } = req;
  let devices = app.get('devices');
  try {
    var result = await devices.insertOne(body);
    console.log(result);
    return result;
  } catch (e) {
    console.log(e);
  }
}


var updateDevice = async () => {
  try {
    await devices.update();
  } catch (e) {
    console.log(e);
  }
}

module.exports = {
  getAllDevices,
  addDevices,
  updateDevice
}
