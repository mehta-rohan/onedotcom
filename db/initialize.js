const MongoClient = require('mongodb').MongoClient
const db_uri = `mongodb://otp:otp_202020@127.0.0.1:27017/otp`

let _db;


// connectDB: connecting db once the server started/restarted
const connectDB = async (callback) => {
  try {
    MongoClient.connect(db_uri, {
      useNewUrlParser: true
    }, (err, client) => {
      console.log(err, client);
      _db = client.db('otp')
      return callback(err); // if there is no error then postive case will work and null passed in callback
    })
  } catch (e) {
    throw e
  }
}

// getDBErrorMsg: It will render the msg from db into more readable form
let getDBErrorMsg = (e) => {
  let err_msg = "Unknow error";
  if (!e) return err_msg;
  if (e && e.code == 11000) {
    for (err of err_field)
      if (e.errmsg.indexOf(err) > -1) {
        err_msg = `Duplicate ${err}`;
        break;
      }
  }
  return err_msg;
}

// getDB: This method is used in extenal files to get access to db object
const getDB = () => _db

// disconnectDB: Name don't lie.
const disconnectDB = () => _db.close()

module.exports = {
  connectDB,
  getDB,
  disconnectDB,
  getDBErrorMsg
}
