var express = require('express');
var router = express.Router();
var {
  addDevices,
  getAllDevices,
  updateDevice
} = require('./../db/devices');

router.get('/list', function (req, res, next) {
  console.log(req.body);
});

router.post('/add', async function (req, res, next) {
  console.log(req.body);

  await addDevices(req);
});

router.delete('/deleteDevice', function (req, res, next) {
  console.log(req.body);
});

router.put('/update', function (req, res, next) {
  console.log(req.body);
});

router.get('/checkStatus', function (req, res, next) {
  console.log(req.body);
});


module.exports = router;
